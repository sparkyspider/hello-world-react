import React from 'react';
import {Button} from "./Button";

export class List extends React.Component {

    handleDelete = (evt, index) => {
        // @TODO: Hot sure how to identify which object was clicked.
        console.log (evt.target);
        console.log (index);
    };

    render() {

        return (
            <ul onClick={this.handleDelete}>
                {this.props.items.map(item => <li key={item.id}>{item.task} <Button title="X"/></li>)}
            </ul>
        )

    }
}
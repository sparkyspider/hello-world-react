import React from 'react';

export class Textbox extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: ""}
    }

    onChange = (e)  => {
        let val = e.target.value;
        this.props.onChange(val);
        this.setState({value: val})


    };

    // @TODO: This text input could really use some validation
    render() {
        return (

            <input type="text" value={this.state.value} onChange={this.onChange}/>

        )
    }
}
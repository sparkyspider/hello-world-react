import React from 'react';

export class Button extends React.Component {

    handleClick = () => {
        if (this.props.onClick)
            this.props.onClick();
    }

    render = () =>
        <span>
            <button onClick={this.handleClick}>{this.props.title}</button>
        </span>;

}
import React from 'react';

/* Using curly brakets to avoid having to instantly return a oneliner */
export const PrettyDate = (props) => {
    let now = new Date();
    return (<span>{now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay()}</span>);
};

/* Sneaky trick to make the smallest component, not even requiring a return statement */
export class UglyDate extends React.Component {
    render = () => <span>{new Date().toString()}</span>
}

/* Handling state and events + showing and hiding stuff */
export class ToggleButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {toggleOn: true, clicks: 0};
    }

    handleClick = () => {
        this.setState(state => ({toggleOn : !state.toggleOn}));
        this.setState(state => ({clicks : state.clicks + 1}));
    };

    render() {
        if (this.state.clicks > 20) return false; // This component must die at 20 clicks.
        return (
            <span>
                <button onClick={this.handleClick} value={this.state.toggleOn}>{this.state.toggleOn ? "On" : "Off"} and {this.state.clicks} clicks</button>
                {this.state.clicks === 0 &&
                    <span>Use this button to toggle the list visibility on and off</span>
                }
            </span>
        )
    }
}

/* Handling State */
export class Clock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            date: new Date() // This is the only place you can set state directly without using the setState method
        };
    }

    componentDidMount() {
        this.timerId = setInterval(() => this.tick(), 1000)
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    tick() {
        this.setState({date: new Date()});
    }

    render() {
        return <span>The time is now: {this.state.date.toLocaleString()}</span>
    }

}
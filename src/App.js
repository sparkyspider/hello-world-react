import React, {Component} from 'react';
import './App.css';
import {PrettyDate, UglyDate, Clock, ToggleButton} from './RandomTags';
import {Textbox} from "./components/Textbox";

class App extends Component {

    render() {

        const user = {
            name: "Mark",
            surname: "van Wyk"
        };

        const heading = (
            <h1>Hello, {formatName(user)}</h1>
        );

        function formatName(user) {
            return user.name + " " + user.surname;
        }

        const Listbox = (props) =>
            <ul>
                {props.items.map(item => <li key={item}>{item}</li>)}
            </ul>
        ;

        return (
            <div>
                {heading}
                <h2>My Listbox</h2>
                <Listbox items={['Mark', 'Tarryn', 'Onno', 'Ginger']}/>
                <Textbox/>
                <hr/>
                <ToggleButton/>
                <p>
                    Please note that this application was refreshed at <PrettyDate/> which can also be seen
                    as <UglyDate/>. The time now, however is <Clock date="2018-01-01"/>
                </p>
                <h1>My comments</h1>
                <ul>
                    <li>Enclose variables in curly braces</li>
                    <li>Do not use quotes when using curly braces.</li>
                    <li>Remember, class is className and everything is in camelCase.</li>
                    <li>Babel translates JSX into React.createElement tags</li>
                    <li>Note how the at arrow works => [1,2,3].map((n)=> n + 1);</li>
                    <li>A React Component can be a simple function (function(props){} or a class extends
                        React.Component)
                    </li>
                    <li>Simple Rendering Example: https://reactjs.org/docs/components-and-props.html</li>
                    <li>React components must be pure functions</li>
                    <li>Only classes can maintain state</li>
                    <li>Except within a constructor always use setState. Never modify states directly.</li>
                    <li>If you are dependent on a state for some calculation, do not ask for the state directly, but
                        rather pass a function.<br/>
                        <code>
                            this.setState((state, props) => (<br/>
                            counter: state.counter + props.increment<br/>
                            ));

                        </code>
                    </li>
                    <li>Do not use the fancy onClick=[() => handleEvent()]. It creates a new binding every time it renders.</li>
                </ul>
                <h2>Also to explore</h2>
                <ul>
                    <li>React Basics - Done</li>
                    <li>Component Basics - Done</li>
                    <li>Basics of Wiring Up Components</li>
                    <li>Build the Listings Application</li>
                    <li>Standard React Styling</li>
                    <li>Advanced React Styling with React Components</li>
                    <li>Routing and Single-Page Applications</li>
                    <li>Effectively Managing State with React Router</li>
                    <li>Redux</li>
                    <li>Productivity Patterns -> Components are cheaper as functions</li>
                    <li>Http / REST patterns</li>
                    <li>Already existing components (Public Component Library)</li>
                </ul>
            </div>
        );
    }
}

export default App;

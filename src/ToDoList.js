import React, {Component} from 'react';
import {Textbox} from "./components/Textbox";
import {Button} from "./components/Button";
import {List} from "./components/List";

/***
 * ToDoList is a React component that allows users to add and remove items from a ToDoList
 */
export default class ToDoList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            textBuffer: ""
        };

        //@TODO: These should really be moved to a database.
        this.tasks = [
            {id:1, task:'Grab a samosa'},
            {id:2, task:'Phone Jacques back'},
            {id:3, task:'Call Tarryn and see what time she is going to be back'},
            {id:4, task:'Chat to Char'},
            {id:5, task:'Do the Guesty stuff tonight'},
        ];
    }

    handleItemDelete = (index) => {
        this.tasks.pop(index);
    };

    handleAddItemClick = () => {
        if (this.state.textBuffer.length > 0) {

            let lastUsedId = Math.max.apply(Math, this.tasks.map(function(o) { return o.id; }));
            this.tasks.push ({id: lastUsedId + 1, task:this.state.textBuffer});

            //@TODO: This does not work...
            this.setState({textBuffer: ''}, () => console.log(this.state));

        }
    };

    handleTextChange = (text) => {
        this.setState({textBuffer: text})
    };

    render() {

        return (
            // @TODO: It should be noted that the onDelete attribute is not functioning. Mark is currently unsure how to implement this.
            <div>
                <h1>Mark's ToDo List:</h1>
                <List items={this.tasks} onDelete={this.handleItemDelete}/>
                <div>
                    <Textbox onChange={this.handleTextChange} value={this.state.textBuffer}/><Button title="Add item" onClick={this.handleAddItemClick}/>
                </div>
            </div>

        )
    }
}